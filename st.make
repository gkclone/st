; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v2.0.5/gk_distro.make

; MODULES ======================================================================

; Standard
projects[gk_bugherd][type] = module
projects[gk_bugherd][subdir] = standard
projects[gk_bugherd][download][type] = git
projects[gk_bugherd][download][url] = git@bitbucket.org:greeneking/gk-bugherd.git
projects[gk_bugherd][download][tag] = 7.x-1.0

projects[gk_competitions][type] = module
projects[gk_competitions][subdir] = standard
projects[gk_competitions][download][type] = git
projects[gk_competitions][download][url] = git@bitbucket.org:greeneking/gk-competitions.git
projects[gk_competitions][download][tag] = 7.x-1.0

projects[gk_locations][type] = module
projects[gk_locations][subdir] = standard
projects[gk_locations][download][type] = git
projects[gk_locations][download][url] = git@bitbucket.org:greeneking/gk-locations.git
projects[gk_locations][download][tag] = 7.x-1.15

projects[gk_members][type] = module
projects[gk_members][subdir] = standard
projects[gk_members][download][type] = git
projects[gk_members][download][url] = git@bitbucket.org:greeneking/gk-members.git
projects[gk_members][download][tag] = 7.x-1.4

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-1.1

projects[gk_rewards][type] = module
projects[gk_rewards][subdir] = standard
projects[gk_rewards][download][type] = git
projects[gk_rewards][download][url] = git@bitbucket.org:greeneking/gk-rewards.git
projects[gk_rewards][download][tag] = 7.x-1.15

projects[gk_tiers][type] = module
projects[gk_tiers][subdir] = standard
projects[gk_tiers][download][type] = git
projects[gk_tiers][download][url] = git@bitbucket.org:greeneking/gk-tiers.git
projects[gk_tiers][download][tag] = 7.x-1.3

; LIBRARIES  ===================================================================

projects[headroom][type] = library
projects[headroom][subdir] = ""
projects[headroom][download][type] = git
projects[headroom][download][url] = https://github.com/WickyNilliams/headroom.js.git
projects[headroom][download][tag] = v0.5.0
