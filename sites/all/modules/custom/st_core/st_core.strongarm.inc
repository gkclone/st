<?php
/**
 * @file
 * st_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function st_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'htmlmail_theme';
  $strongarm->value = 'seasonticket';
  $export['htmlmail_theme'] = $strongarm;

  return $export;
}
