<?php
/**
 * @file
 * st_rewards.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function st_rewards_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
