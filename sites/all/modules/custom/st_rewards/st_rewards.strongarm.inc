<?php
/**
 * @file
 * st_rewards.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function st_rewards_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_confirm_details_redirect_path_competition_reward';
  $strongarm->value = 'welcome';
  $export['gk_rewards_confirm_details_redirect_path_competition_reward'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_competition_reward_anonymous';
  $strongarm->value = 'thank-you';
  $export['gk_rewards_signup_redirect_path_competition_reward_anonymous'] = $strongarm;

  return $export;
}
