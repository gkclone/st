<?php
/**
 * @file
 * Code for the Rewards feature.
 */

include_once 'st_rewards.features.inc';

/**
 * Implements hook_block_info().
 */
function st_rewards_block_info() {
  $blocks['st_rewards_signup_reward'] = array(
    'info' => t('Season Ticket Rewards: Sign-up reward'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function st_rewards_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'st_rewards_signup_reward':
      if ($reward_id = variable_get('st_rewards_signup_reward_id')) {
        $block['subject'] = '';
        $block['content'] = drupal_get_form('gk_rewards_user_details_form', NULL, reward_load($reward_id));
      }
    break;
  }

  return $block;
}

/**
 * Implements hook_form_alter().
 */
function st_rewards_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'gk_rewards_user_details_form' && $form_state['gk_rewards']['reward']->type == 'competition_reward') {
    // Add anchor to form action if we are on the home page so the form is
    // visible if there is an error.
    if (drupal_is_front_page()) {
      $form['#action'] = '/#register-now';
    }

    // Add modifier class to form.
    $form['#attributes']['class'][] = 'Form--primary';

    // Re-arrange name fields into a grid...
    $form['name']['#theme_wrappers'] = array('minima_grid');
    unset($form['name']['#type']);

    // ...name label.
    $form['name']['label']['#type'] = 'item';
    $form['name']['label']['#title'] = t('Name');
    $form['name']['label']['#required'] = TRUE;
    $form['name']['label']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
    $form['name']['label']['#grid_cell_attributes'] = array('class' => array('u-alignTop u-size1of4'));
    unset($form['name']['label']['#prefix']);

    // ...name fields.
    $form['name']['fields'] = array(
      'title' => $form['name']['title'],
      'first_name' => $form['name']['first_name'],
      'last_name' => $form['name']['last_name'],
      '#theme_wrappers' => array('minima_grid', 'minima_grid_cell'),
      '#grid_attributes' => array('class' => array('Grid--withGutter')),
      '#grid_cell_attributes' => array('class' => array('u-size3of4')),
    );
    unset($form['name']['title']);
    unset($form['name']['first_name']);
    unset($form['name']['last_name']);

    // ...title.
    $form['name']['fields']['title']['#theme_wrappers'] = array('minima_grid_cell');
    $form['name']['fields']['title']['#grid_cell_attributes'] = array('class' => array('u-size4of16', 'u-sm-sizeFull'));
    $form['name']['fields']['title']['#attributes']['required'] = TRUE;

    // ...first name.
    $form['name']['fields']['first_name']['#theme_wrappers'] = array('minima_grid_cell');
    $form['name']['fields']['first_name']['#grid_cell_attributes'] = array('class' => array('u-size6of16', 'u-sm-sizeFull'));
    $form['name']['fields']['first_name']['#attributes']['required'] = TRUE;

    // ...last name.
    $form['name']['fields']['last_name']['#theme_wrappers'] = array('minima_grid_cell');
    $form['name']['fields']['last_name']['#grid_cell_attributes'] = array('class' => array('u-size6of16', 'u-sm-sizeFull'));
    $form['name']['fields']['last_name']['#attributes']['required'] = TRUE;

    // Set grid cell classes and attributes for other fields...

    // ...email.
    $form['mail']['#title_grid_cell_attributes'] = array('class' => array('u-size1of4'));
    $form['mail']['#field_grid_cell_attributes'] = array('class' => array('u-size3of4'));
    $form['mail']['#attributes']['placeholder'] = 'E.g. yourname@example.com';
    $form['mail']['#attributes']['required'] = TRUE;
    unset($form['confirm_mail']);

    // ...postcode.
    $form['postcode']['#title_grid_cell_attributes'] = array('class' => array('u-size1of4'));
    $form['postcode']['#field_grid_cell_attributes'] = array('class' => array('u-size3of4'));
    $form['postcode']['#attributes']['placeholder'] = 'E.g. IP33 1QT';
    $form['postcode']['#attributes']['placeholder'] = 'E.g. IP33 1QT';
    $form['postcode']['#attributes']['required'] = TRUE;
    $form['postcode']['#attributes']['pattern'] = '[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? ?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}';

    // ...date of birth.
    $form['date_of_birth']['#title_grid_cell_attributes'] = array('class' => array('u-size1of4'));
    $form['date_of_birth']['#field_grid_cell_attributes'] = array('class' => array('u-size3of4'));

    // ...card number.
    $form['card_number'] = array(
      '#type' => 'textfield',
      '#title' => 'Card number',
      '#title_grid_cell_attributes' => array('class' => array('u-size1of4')),
      '#field_grid_cell_attributes' => array('class' => array('u-size3of4')),
      '#attributes' => array(
        'placeholder' => 'xxxx xxxx xxxx xxxx',
        'required' => TRUE,
        'pattern' => '([0-9]{2,4} ?){4}',
      ),
      '#required' => TRUE,
    );

    // ...location nid.
    $location_nid_options = array();

    foreach (gk_locations_get_items() as $node) {
      $location = '';
      if (!empty($node->field_location_address[LANGUAGE_NONE][0])) {
        $address = $node->field_location_address[LANGUAGE_NONE][0];
        $location = $address['administrative_area'] . ' - ' . $address['locality'];
      }
      ksort($location_nid_options);
      $location_nid_options[$location][$node->nid] = $node->title;
    }

    $form['location_nid'] = array(
      '#type' => 'select',
      '#title' => 'Your local',
      '#options' => $location_nid_options,
      '#empty_option' => 'Tell us your local...',
      '#required' => TRUE,
      '#title_grid_cell_attributes' => array('class' => array('u-size1of4')),
      '#field_grid_cell_attributes' => array('class' => array('u-size3of4')),
    );

    // Set value for comms opt-in and remove from form.
    $form['comms_opt_in_other']['#type'] = 'value';
    $form['comms_opt_in_other']['#value'] = 1;

    // Output submit as a 'button' element and set text.
    $form['actions']['submit']['#button_type'] = 'button';
    $form['actions']['submit']['#value'] = t('Register now');
    $form['actions']['#theme_wrappers'] = array();

    // Add custom validate handler.
    $form['#validate'][] = 'st_rewards_user_details_form_validate';
  }
}

/**
 * Validation handler for the user details form.
 */
function st_rewards_user_details_form_validate($form, &$form_state) {
  $range_min = 2562001002002;
  $range_max = 2562009999999;

  $card_number = $form_state['values']['card_number'];

  if (!is_numeric($card_number) || $card_number < $range_min || $card_number > $range_max) {
    form_set_error('card_number', t('Invalid card number.'));
  }
  else {
    $query = db_select('gk_rewards_user_details', 'ud')
      ->condition('data', "%$card_number%", 'LIKE')->countQuery();

    if ($query->execute()->fetchField()) {
      form_set_error('card_number', t('Card number already registered.'));
    }
    else {
      $form_state['values']['data']['card_number'] = $card_number;
      $form_state['values']['data']['location_nid'] = $form_state['values']['location_nid'];
    }
  }
}

/**
 * Preprocess variables for the htmlmail template.
 */
function st_rewards_preprocess_htmlmail(&$variables) {
  if ($variables['module'] == 'gk_rewards' && $variables['key'] == 'confirm_details') {
    $variables['banner'] = theme('image', array(
      'path' => drupal_get_path('theme', 'seasonticket') . '/images/please-confirm-your-details.jpg',
      'alt' => 'Please confirm your details',
    ));
  }
}

/**
 * Preprocess function for the "confirm details" mail template.
 */
function st_rewards_preprocess_gk_rewards_confirm_details_mail(&$variables) {
  $variables['intro'] = 'Have we got the right person?';

  $variables['summary']  = 'Please follow the link below to confirm your details.<br />';
  $variables['summary'] .= 'Once you\'ve clicked you will then be entered into a price draw, with<br />the chance to win...<br /><br />';
  $variables['summary'] .= '<strong>England Season Tickets</strong><br />';
  $variables['summary'] .= '<strong>A signed, framed England shirt</strong><br />';
  $variables['summary'] .= '<strong>1 of 100 Carlsberg England shirts</strong><br />';

  $variables['confirm_details_text'] = theme('image', array(
    'path' => drupal_get_path('theme', 'seasonticket') . '/images/confirm-you-details-CTA.jpg',
    'alt' => 'Confirm your details',
  ));
}

/**
 * Implements gk_rewards_user_details_csv_export_headers_alter().
 */
function st_rewards_gk_rewards_user_details_csv_export_headers_alter(&$csv_headers) {
  $csv_headers += array(
    'card_number' => array(
      'title' => 'Card number',
      'value callback' => 'st_rewards_user_details_csv_export_value_callback__card_number',
      'weight' => 200,
    ),
    'location_nid' => array(
      'title' => 'Location',
      'value callback' => 'st_rewards_user_details_csv_export_value_callback__location_nid',
      'weight' => 300,
    ),
    'location_house_id' => array(
      'title' => 'House ID',
      'value callback' => 'st_rewards_user_details_csv_export_value_callback__location_house_id',
      'weight' => 400,
    ),
  );
}

/**
 * User details CSV export value callback: Card number
 */
function st_rewards_user_details_csv_export_value_callback__card_number($data_value, $user_details) {
  if (!empty($user_details->data['card_number'])) {
    $card_number = $user_details->data['card_number'];
    unset($user_details->data['card_number']);
    return $card_number;
  }
}

/**
 * User details CSV export value callback: Location node ID
 */
function st_rewards_user_details_csv_export_value_callback__location_nid($data_value, $user_details) {
  if (empty($user_details->data['location_nid'])) {
    return NULL;
  }

  static $location_nodes = NULL;

  if (!isset($location_nodes)) {
    $location_nodes = gk_locations_get_items();
  }

  if (isset($location_nodes[$user_details->data['location_nid']])) {
    return $location_nodes[$user_details->data['location_nid']]->title;
  }
}

/**
 * User details CSV export value callback: Location house ID
 */
function st_rewards_user_details_csv_export_value_callback__location_house_id($data_value, $user_details) {
  if (empty($user_details->data['location_nid'])) {
    return NULL;
  }

  static $location_nodes = NULL;

  if (!isset($location_nodes)) {
    $location_nodes = gk_locations_get_items();
  }

  if (isset($location_nodes[$user_details->data['location_nid']]) && $house_id = field_get_items('node', $location_nodes[$user_details->data['location_nid']], 'field_location_house_id')) {
    unset($user_details->data['location_nid']);
    return $house_id[0]['value'];
  }
}
