<?php

/**
 *
 */
abstract class LocationMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Olly Nevard', 'olivernevard@greeneking.com',
                            t('Lead UI Developer')),
      new MigrateTeamMember('Tom Cant', 'tomcant@greeneking.co.uk',
                            t('Lead Backend Developer')),
    );

    //$this->issuePattern = 'http://drupal.org/node/:id:';
  }
}

/**
 *
 */
class LocationNodeMigration extends LocationMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate locations from a CSV to nodes');

    // Define CSV columns.
    $columns = array(
      array('house_id', 'House ID'),
      array('title', 'Title'),
      array('address_1', 'Address 1'),
      array('town', 'Town'),
      array('county', 'County'),
      array('postcode', 'Postcode'),
      array('telephone', 'Telephone'),
    );
    $path = drupal_get_path('module', 'st_locations') . '/csv/locations.csv';

    // Define CSV options.
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    // Define source and destination.
    $this->source = new MigrateSourceCSV($path, $columns, $options);
    $this->destination = new MigrateDestinationNode('location');

    // Define map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'house_id' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'House ID',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Add field mappings.
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('status')->defaultValue(NODE_PUBLISHED);
    $this->addFieldMapping('field_location_house_id', 'house_id');
    $this->addFieldMapping('field_location_telephone', 'telephone');

    $this->addUnmigratedSources(array(
      'address_1',
      'town',
      'county',
      'postcode',
    ));

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'uid',
      'created',
      'changed',
      'promote',
      'sticky',
      'revision',
      'log',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'is_new',
      'body',
      'body:format',
      'body:summary',
      'body:language',
      'field_location_email',
      'field_location_address',
      'field_location_address:administrative_area',
      'field_location_address:sub_administrative_area',
      'field_location_address:locality',
      'field_location_address:dependent_locality',
      'field_location_address:postal_code',
      'field_location_address:thoroughfare',
      'field_location_address:premise',
      'field_location_address:sub_premise',
      'field_location_address:organisation_name',
      'field_location_address:name_line',
      'field_location_address:first_name',
      'field_location_address:last_name',
      'field_location_address:data',
      'field_location_facilities',
      'field_location_facilities:source_type',
      'field_location_facilities:create_term',
      'field_location_facilities:ignore_case',
      'field_location_fax',
      'field_location_fax:language',
      'field_location_geo',
      'field_location_geo:geo_type',
      'field_location_geo:lat',
      'field_location_geo:lon',
      'field_location_geo:left',
      'field_location_geo:top',
      'field_location_geo:right',
      'field_location_geo:bottom',
      'field_location_geo:geohash',
      'field_location_images',
      'field_location_images:file_class',
      'field_location_images:language',
      'field_location_images:preserve_files',
      'field_location_images:destination_dir',
      'field_location_images:destination_file',
      'field_location_images:file_replace',
      'field_location_images:source_dir',
      'field_location_images:urlencode',
      'field_location_images:alt',
      'field_location_images:title',
      'field_location_opening_hours',
      'field_location_opening_hours:starthours',
      'field_location_opening_hours:endhours',
      'field_location_food_hours',
      'field_location_food_hours:starthours',
      'field_location_food_hours:endhours',
      'field_location_house_id:language',
      'field_location_telephone:language',
      'path',
      'comment',
      'metatag_refresh',
      'metatag_title',
      'metatag_description',
      'metatag_abstract',
      'metatag_keywords',
      'metatag_robots',
      'metatag_news_keywords',
      'metatag_standout',
      'metatag_generator',
      'metatag_copyright',
      'metatag_image_src',
      'metatag_canonical',
      'metatag_shortlink',
      'metatag_publisher',
      'metatag_author',
      'metatag_original-source',
      'metatag_revisit-after',
    ));
  }

  /**
   *
   */
  public function prepare($node, stdClass $row) {
    // Address.
    $node->field_location_address[LANGUAGE_NONE][] = array(
      'country' => 'GB',
      'thoroughfare' => $row->address_1,
      'premise' => '',
      'locality' => $row->town,
      'administrative_area' => $row->county,
      'postal_code' => $row->postcode,
    );
  }
}
