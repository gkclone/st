<div class="Grid-cell">
  <div class="Box Box--locationListCounty">
    <div class="Grid Grid--withGutter">
      <div class="Grid-cell u-xl-size1of3">
        <h2 class="Box-title">
          <?php echo $group_title ?>
        </h2>
      </div>

      <div class="Grid-cell u-xl-size2of3">
        <div class="Box-content">
          <div class="Grid Grid--withGutter">
            <?php foreach ($localities as $title => $locations): ?>
              <div class="Grid-cell u-xl-size1of3 u-lg-size1of3 u-md-size1of2 ">
                <div class="Box Box--locationListLocality">
                  <h3 class="Box-title">
                    <?php echo $title ?>
                  </h3>

                  <ul class="Box-content">
                    <?php foreach ($locations as $location): ?>
                      <li><?php echo $names[$location->nid]; ?></li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>

      <div class="Grid-cell">
        <hr />
      </div>
    </div>
  </div>
</div>
