(function($) {
  Drupal.behaviors.st = {
    attach: function (context, settings) {
			/**
			 * Cookies message.
			 */
			$('.CookiesMessage-continue').click(function(e) {
				e.preventDefault();
				$('.Container--cookiesMessage').hide();
			});

			/**
			 * Smooth scroll anchor links.
			 * TODO: Convert this into a plugin for minima.
			 */
			$('body').on('click', 'a[href*=#]:not([href=#])', function(e) {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					e.preventDefault();

					var offset = 0;
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
							scrollTop: target.offset().top - offset
						}, 700);
					}
					// Close off canvas.
					$.OffCanvas('close');
				}
			});

			/**
			 * Location toggle.
			 */
			var locations = $('.Container--locations').hide();

			var locationToggle = $('<div class="Grid-cell u-size1of4 u-xs-sizeFull u-sm-sizeFull u-md-sizeFull u-textRight">')
				.appendTo($('.Container--find .Grid'));

			$('<a class="LocationToggle" href="#">See all participating pubs <i class="Icon Icon--right Icon--downArrow a-rotateLeft90"></i></a></div>')
				.click(function(e) {
					e.preventDefault();
					$(this).find('.Icon').toggleClass('a-rotateLeft90 a-rotateRight90');
					locations.slideToggle();
				})
				.appendTo(locationToggle);

			/**
			 * Initialise headroom.js on header.
			 */
			$(".Container--header")
				.attr('data-offcanvas-fixed', 'top')
				.headroom({
					"offset": 65,
					"tolerance": 5,
					"classes": {
						"initial": "is-animated",
						"pinned": "a-slideDown",
						"unpinned": "a-slideUp",
						"top": "is-top",
						"notTop": "is-notTop"
					}
				});

			/**
			 * Back to top link.
			 */
			$('.Nav-item--backToTop').hide();

			$(window).scroll(function () {
				if ($(this).scrollTop() > 500) {
						$('.Nav-item--backToTop').fadeIn();
				} else {
						$('.Nav-item--backToTop').fadeOut();
				}
			});

			/**
			 * Off-canvas.
			 */
			$.OffCanvas({
				maxWidth: '992px',
				panels: {
					'primary' : {
						'side' : 'right',
						'size' : 200
					}
				}
			});

			/**
			 * AlertBoxes.
			 */
			$.AlertBox();

			var data = {}

			data.offCanvasToggle = $(
				'<div class="Grid-cell u-size1of4 u-textRight">' +
					'<button class="OffCanvasToggle" data-offcanvas-handle="primary">' +
  					'<span class="OffCanvasToggle-lines"></span>' +
					'</button>' +
				'</div>')
				.appendTo($('.Container--header .Grid'));

			if (window.matchMedia !== undefined) {
				data.mediaQuery = window.matchMedia('(max-width: 992px)');
				data.mediaQuery.addListener(_onRespond);
				_onRespond();
			}

			function _onRespond() {
				if (data.mediaQuery.matches) {
					data.offCanvasToggle.show();
				} else {
					data.offCanvasToggle.hide();
				}
			}
    }
  }
})(jQuery);
