<?php

// Plugin definition
$plugin = array(
  'title' => t('Page ST Home'),
  'category' => t('Page'),
  'theme' => 'minima_page_layout',
  'regions' => array(
    'welcome' => t('Welcome'),
    'intro' => t('Introduction'),
    'prizes' => t('Great prizes'),
    'discounts' => t('Great discounts'),
    'locations' => t('Locations'),
    'register' => t('Register your card'),
    'follow' => t('Follow the action on Facebook'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_page_st_home($variables) {
  return array(
    'welcome' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--welcome'),
      ),
      'welcome' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-textCenter'),
        ),
        '#markup' => $variables['content']['welcome'],
      ),
    ),
    'intro' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--intro'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'intro' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['intro'],
      ),
    ),
    'prizes' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--prizes', 'u-textCenter'),
        'id' => 'great-prizes',
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter', 'Grid--alignMiddle'),
      ),
      'prizes' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['prizes'],
      ),
    ),
    'discounts' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--discounts'),
        'id' => 'great-discounts',
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'discounts' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['discounts'],
      ),
    ),
    'find' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--find'),
        'id' => 'find-my-local',
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--alignMiddle'),
      ),
      'find' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-size3of4', 'u-xs-sizeFull', 'u-sm-sizeFull', 'u-md-sizeFull'),
        ),
        '#markup' => '<h2>Where Can I Use My Season Ticket Card?</h2>',
      ),
    ),
    'locations' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--locations'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'locations' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['locations'],
      ),
    ),
    'register' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--register'),
        'id' => 'register-now',
      ),
      '#grid_attributes' => array(
        'class' => array('Grid-cell--center', 'u-xl-size3of4'),
      ),
      'register' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['register'],
      ),
    ),
    'follow' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--follow'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'follow' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['follow'],
      ),
    ),
  );
}
