<?php

// Plugin definition
$plugin = array(
  'title' => t('Page ST Landing'),
  'category' => t('Page'),
  'theme' => 'minima_page_layout',
  'regions' => array(
    'primary' => t('Primary'),
    'locations' => t('Locations'),
    'follow' => t('Follow the action on Facebook'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_page_st_landing($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['primary'],
      ),
    ),
    'find' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--find'),
        'id' => 'find-my-local',
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--alignMiddle'),
      ),
      'find' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-size3of4'),
        ),
        '#markup' => '<h2>Where Can I Use My Season Ticket Card?</h2>',
      ),
    ),
    'locations' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--locations'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'locations' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['locations'],
      ),
    ),
    'follow' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--follow'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'follow' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['follow'],
      ),
    ),
  );
}
