<?php

// Plugin definition
$plugin = array(
  'title' => t('Site ST Default'),
  'category' => t('Site'),
  'icon' => 'site_st_default.png',
  'theme' => 'minima_site_layout',
  'regions' => array(
    'branding' => t('Branding'),
    'navigation' => t('Navigation'),
    'content' => t('Content'),
    'footer' => t('Footer'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_site_st_default($variables) {
  $layout = array(
    'header_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'header',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--header'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'branding' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-size3of4', 'u-lg-size1of3', 'u-xl-size1of3'),
        ),
        '#markup' => $variables['content']['branding'],
      ),
      'navigation' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-lg-size2of3', 'u-xl-size2of3'),
          'data-offcanvas-panel' => 'primary',
        ),
        '#markup' => $variables['content']['navigation'],
      ),
    ),
    'main' => array(
      '#markup' => $variables['content']['content'],
    ),
    'footer_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'footer',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--footer'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'footer' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-textCenter'),
        ),
        '#markup' => $variables['content']['footer'],
      ),
    ),
  );

  // If current page isn't handled by page manager wrap content in a container.
  $current_page = page_manager_get_current_page();
  if (empty($current_page)) {
    $layout['main'] = array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'main' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        'content_header' => array(
          '#theme' => 'pane_content_header'
        ),
        'content' => array(
          '#markup' => $variables['content']['content'],
        ),
      ),
    );
  }

  return $layout;
}
