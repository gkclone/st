<?php

/**
 * Preprocess variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function seasonticket_preprocess_html(&$variables, $hook) {
  // Load Headroom JS plugin.
  drupal_add_js(libraries_get_path('headroom') . '/dist/headroom.min.js');
  drupal_add_js(libraries_get_path('headroom') . '/dist/jQuery.headroom.min.js');

  // Load Minima plugins.
  drupal_add_js(libraries_get_path('minima') . '/dist/js/minima.alertbox.min.js');
  drupal_add_js(libraries_get_path('minima') . '/dist/js/minima.offcanvas.min.js');

  // Load fonts from Fonts.com.
  global $is_https;
  $protocol = $is_https ? 'https' : 'http';
  drupal_add_js($protocol . '://fast.fonts.net/jsapi/740be2aa-aebd-4256-ac60-f7ae1cd0bbe4.js', 'external');
}

/**
 * Implements hook_preprocess_pane_content_header().
 */
function seasonticket_preprocess_pane_content_header(&$variables) {
  // Disable breadcrumb.
  $variables['breadcrumb'] = '';
}

/**
 * Preprocess variables for panels-pane.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("panels_pane" in this case).
 */
function seasonticket_preprocess_panels_pane(&$variables, $hook) {
  // Display only the content for the following pane types.
  $content_only_panes = array(
    'menu_block-gk-core-footer-menu',
    'menu_block-gk-core-main-menu',
    'gk_core-copyright'
  );

  if (in_array($variables['pane']->subtype, $content_only_panes)) {
    $variables['theme_hook_suggestion'] = 'box__content_only';
  }

  // Add main menu to primary off-canvas panel.
  if ($variables['pane']->subtype == 'menu_block-gk-core-main-menu') {
    $variables['grid_attributes_array']['data-offcanvas-panel'] = 'primary';
  }

  // Add 'Box--primary' class to register form.
  if ($variables['pane']->subtype == 'st_rewards-st_rewards_signup_reward') {
    $variables['attributes_array']['class'][] = 'Box--primary';
  }

  // Add 'Box--primary' class to register form.
  if ($variables['pane']->subtype == 'st_rewards-st_rewards_signup_reward') {
    $variables['attributes_array']['class'][] = 'Box--primary';
  }
}

/**
 * Implements hook_preprocess_menu_link().
 */
function seasonticket_preprocess_menu_link(&$variables) {
  $element =& $variables['element'];

  // Add Facebook icon to primary menu.
  if ($element['#original_link']['menu_name'] == 'main-menu' && $element['#title'] == 'Facebook') {
    $element['#attributes']['class'][] = 'Nav-item--icon';
    $element['#localized_options']['attributes']['title'] = t('Follow us on Facebook');
    $element['#localized_options']['html'] = TRUE;
    $element['#title'] = '<span class="is-hiddenVisually">Facebook</span><i class="Icon Icon--facebook"></i>';
  }
}
