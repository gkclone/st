<div class="Container Container--cookiesMessage" data-offcanvas-fixed="bottom">
  <div class="Container-inner">
    <div class="CookiesMessage">
      <div class="Grid Grid--withGutter Grid--alignMiddle">
        <div class="Grid-cell u-size3of4 u-xs-sizeFull u-sm-sizeFull">
          <?php if (!empty($title)): ?>
            <h2 class="CookiesMessage-title"><?php print $title; ?></h2>
          <?php endif; ?>

          <div class="CookiesMessage-message">
            <?php print $message; ?>
          </div>
        </div>

        <div class="Grid-cell u-size1of4 u-xs-sizeFull u-sm-sizeFull">
          <ul class="CookiesMessage-options">
            <?php if (!empty($more_link)): ?>
              <li class="cookies__more"><?php print $more_link; ?></li>
            <?php endif; ?>
            <li class="CookiesMessage-continue">
              <a class="Button Button--small" href="/">Continue</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
